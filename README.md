# Fantasy battles
## Základní funkce
Klient se po spuštění aplikace dostane do velmi omezené nabídky, odkud bude schopen se
připojit k serveru (po kliknutí se otevře nové okno s možností zadat uživatelské jméno a
heslo), zobrazit základní informace o aplikaci a ukončit aplikaci. Také zde již bude k vidění
konzole, do které se vkládají jak požadavky klienta, tak odpovědi serveru.

Po úspěšném připojení na server má klient přístup k hlavnímu oknu aplikace. Má k dispozici
funkce úpravy své virtuální postavy. Tak se otevře nové okno s možností výběru typu
postavy (válečník, lovec, rytíř, kouzelník), výběr kouzel a dovedností. Tuto postavu si je
možné pojmenovat podle sebe (nesouvisí s uživatelským jménem) a ukládá se lokálně, na
straně klienta. Po dokončení úprav postavy je na výběr funkce k nalezení protivníka k bitvě.
Zde se na server odešle klientova virtuální postava a server tuto postavu schválí a začne
hledat vhodného protivníka.

Po nalezení protihráče se vlevo zobrazí hráčova postava, vpravo postava protivníka, a
zpřístupní se bojové funkce - útok, obrana, dovednost, vzdát se.
Bitva končí tehdy, když zdraví (HP) jednoho z hráčů dosáhne 0. Po ukončení bitvy se klient
zase může věnovat úpravě své postavy a nebo najít dalšího protihráče.

## Komunikační protokol
Server na každý příkaz odpovídá zprávami ve formátu:\
`OK; odpověď` v případě, že příkaz byl správně zpracován,\
`INFO; odpověď` v případě, že příkaz byl správně zpracován a odpověď je informativního charakteru (např. průběh bitvy),\
`ERROR; odpověď` v případě, že příkaz byl zpracován s chybou.

### Příkazy

* LOGIN 
* ABOUT
* QUIT
* CHAR
    * NAME
    * ROLE
    * SKILL
* BATTLE FIND / BATTLE CREATE
    * ATT
    * DEF
    * SKILL
    * RESIGN



#### LOGIN

Příkaz pro zadání uživatelského jména a hesla.\
`LOGIN username password`


#### ABOUT

Příkaz pro výpis informací o počtu přihlášených uživatelů a hráčích hledajících bitvu.\
`ABOUT`


#### QUIT

Příkaz pro odpojení se od serveru.\
`QUIT`

#### CHAR

Příkaz, kterým se zapne sekvence vytvoření charakteru. Musí následovat příkazy pro tento mód.\
`CHAR`

Příkazy vytváření charakteru:
1. #### NAME
* `NAME name`
* Příkaz pro nastavení jména postavy. 

2. #### ROLE
* `ROLE [WARRIOR, ROGUE, KNIGHT, MAGE]`
* Příkaz pro nastavení typu postavy.

3. #### SKILL
* `SKILL [SHIELD_BASH, CHARGE; RIPOSTE, ASSASSINATE; DIVINE_SHIELD, JUDGEMENT; FIREBALL, DRAGON_BREATH]`
* Příkaz pro nastavení dovednosti. Lze mít jen jednu ze dvou dovedností dostupných pro každou jednotlivou ROLE. To znamená, že MAGE nemůže zvolit např. CHARGE, ale buď FIREBALL nebo DRAGON_BREATH.

4. #### EXIT
* `EXIT`
* Příkaz pro ukončení módu vytváření postavy.

#### BATTLE FIND / BATTLE CREATE

Příkaz pro vstup do módu bitvy. Uživatel, který musí mít vytvořenou a správně nastavenou postavu, může buď bitvu hledat, a pokud nějaký jiný uživatel má bitvu vytvořenou a čeká na dalšího hráče, hráči se spojí v jednu bitvu.\
Pokud nikdo jiný bitvu nehledá, může hrát bitvu vytvořit a čekat na připojení jiného hráče.\
`BATTLE FIND`
`BATTLE CREATE`

Příkazy bitevního módu:
1. #### ATT
* `ATT`
* Příkaz pro provedení útoku na protivníka.

2. #### DEF
* `DEF`
* Příkaz pro zaujetí obranné pozice a zvýšení obranných schopností.

3. #### SKILL
* `SKILL`
* Příkaz pro použití vybrané dovednosti.

4. #### RESIGN
* `RESIGN`
* Příkaz pro vzdání se a opuštění bitvy.




