package cz.vse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 *  The Listener class takes care of listening for incoming connections.
 *  If a connection is accepted, a new instance of Connection class is made
 *  and a thread is created on that instance.
 *
 *@author     Jiri Mares
 *@since    november 2020
 */

public class Listener {
    private ServerSocket server;
    private ArrayList<Connection> connectionList;
    final Logger log = LoggerFactory.getLogger(Listener.class);

    public Listener (int port) {
        this.connectionList = new ArrayList<>();

        try {
            server = new ServerSocket(port);
            log.info("Server connected to at port {}!", port);

        } catch (IOException e) {
            e.printStackTrace();
            log.error("Failed to start server at port {}!", port);
        }
    }

    /**
     * Server is listening to new connections in a while cycle. When a new connection is made,
     * a new instance of a Connection class is created and a new thread is created on that instance.
     */
    public void acceptConnections() throws IOException {
        try {
            log.info("Awaiting connections ...");

            while (true) {
                Connection connection = new Connection(this);
                connection.setSocket(server.accept());
                Thread connection_t = new Thread(connection, "connection");
                log.info("Incoming connection accepted!");
                addToConnectionList(connection);
                connection_t.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
            server.close();
            log.error("Failed to accept connection!");
        }
    }

    public ServerSocket getServer() {
        return server;
    }

    public void setServer(ServerSocket server) {
        this.server = server;
    }

    public synchronized ArrayList<Connection> getConnectionList() {
        return connectionList;
    }

    public void setConnectionList(ArrayList<Connection> connectionList) {
        this.connectionList = connectionList;
    }

    public void addToConnectionList(Connection connection) {
        this.connectionList.add(connection);
    }

    public synchronized void removeFromConnectionList(Connection connection) {
        this.connectionList.remove(connection);
    }

    /**
     * Returns a String with all usernames connected to the server separated by a newline.
     * If a user hasn't logged in yet, the username is Anonymous.
     *
     * @return String with all usernames separated by a newline
     */
    public synchronized String getAllUsernames() {
        String allUsernames = "";

        for (Connection connection : this.getConnectionList()) {
            if (connection.getPlayer() != null) {
                allUsernames += connection.getPlayer().getUsername();
                allUsernames += "\n";
            } else {
                allUsernames += "Anonymous";
                allUsernames += "\n";
            }
        }

        return allUsernames.stripTrailing(); // to get rid of the last newline
    }

    /**
     * Returns a String with all usernames connected to the server and looking for a battle separated by a newline.
     *
     * @return String with all usernames separated by a newline
     */
    public synchronized String getAllLookingForBattle() {
        String allUsernames = "";

        for (Connection connection : this.getConnectionList()) {
            if (connection.lookingForBattle) {
                allUsernames += connection.getPlayer().getUsername();
                allUsernames += "\n";
            }
        }

        return allUsernames.stripTrailing(); // to get rid of the last newline
    }

    /**
     * Returns an integer representing the number of all connected users to the server.
     *
     * @return an integer for all users
     */
    public synchronized int countUsers() {
        int userCount = 0;

        for (Connection connection : this.getConnectionList()) {
            userCount++;
        }

        return userCount;
    }


}
