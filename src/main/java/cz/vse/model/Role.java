package cz.vse.model;

/**
 * The Role class represents the roles available for his created character at character creation and at battles.
 *
 *@author     Jiri Mares
 *@since    november 2020
 */

public class Role {
    private RoleName roleName;
    private int health;
    private int mana;
    private int strength;
    private int agility;
    private int endurance;
    private int intellect;

    /**
     * This constructor sets all the properties based on the roleName.
     *
     * @param roleName a RoleName enum
     */
    public Role(RoleName roleName) {
        this.roleName = roleName;

        switch (roleName) {
            case WARRIOR:
                this.health = 200;
                this.mana = 50;
                this.strength = 10;
                this.agility = 5;
                this.endurance = 5;
                this.intellect = 1;
                break;

            case ROGUE:
                this.health = 100;
                this.mana = 50;
                this.strength = 5;
                this.agility = 10;
                this.endurance = 5;
                this.intellect = 1;
                break;

            case KNIGHT:
                this.health = 150;
                this.mana = 100;
                this.strength = 5;
                this.agility = 1;
                this.endurance = 10;
                this.intellect = 5;
                break;

            case MAGE:
                this.health = 100;
                this.mana = 200;
                this.strength = 1;
                this.agility = 5;
                this.endurance = 5;
                this.intellect = 10;
                break;
        }
    }

    public RoleName getRoleName() {
        return roleName;
    }

    public void setRoleName(RoleName roleName) {
        this.roleName = roleName;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getIntellect() {
        return intellect;
    }

    public void setIntellect(int intellect) {
        this.intellect = intellect;
    }
}
