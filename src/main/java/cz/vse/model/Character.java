package cz.vse.model;

/**
 * The Character class represents the client's created avatar which he can use for a battle against another player.
 *
 *@author     Jiri Mares
 *@since    november 2020
 */


public class Character {
    private String name;
    private Role role;
    private Ability skill;
    private int health;
    private int mana;
    private int strength;
    private int agility;
    private int endurance;
    private int intellect;

    private int defense;        // DEF, DIVINE SHIELD
    private boolean stunned;    // SHIELD BASH
    private boolean charged;    // CHARGE
    private boolean riposte;    // RIPOSTE
    private int assassinate;    // ASSASSINATE - number of rounds of the 3 rounds crit effect

    private int crit;

    public Character() {
        this.name = null;
        this.role = null;
        this.skill = null;
    }

    /**
     * This constructor is used for deep copying of objects of Character class. In the program it is used for
     * backing up the character so the changes made in a battle are only temporary.
     *
     * @param character a character to copy
     */
    public Character(Character character) {
        this.name = character.getName();
        this.setRole(character.getRole());  // v set role mame dalsi dulezite funkce
        this.setSkill(character.getSkill());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;

        this.health = role.getHealth();
        this.mana = role.getMana();
        this.strength = role.getStrength();
        this.agility = role.getAgility();
        this.endurance = role.getEndurance();
        this.intellect = role.getIntellect();

        this.crit = this.agility * 3;
        this.defense = this.endurance * 3;

        this.assassinate = -1;
    }

    public Ability getSkill() {
        return skill;
    }

    public void setSkill(Ability skill) {
        this.skill = skill;
    }

    /**
     * This method is used to check if the character creation process was successful and the character has all
     * needed properties.
     *
     * @return true if the character is properly created, false if not
     */
    public Boolean isAllSet() {
        return this.name != null
                && this.role != null
                && this.skill != null;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getIntellect() {
        return intellect;
    }

    public void setIntellect(int intellect) {
        this.intellect = intellect;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public boolean isStunned() {
        return stunned;
    }

    public void setStunned(boolean stunned) {
        this.stunned = stunned;
    }

    public boolean isCharged() {
        return charged;
    }

    public void setCharged(boolean charged) {
        this.charged = charged;
    }

    public boolean isRiposte() {
        return riposte;
    }

    public void setRiposte(boolean riposte) {
        this.riposte = riposte;
    }

    public int getAssassinate() {
        return assassinate;
    }

    public void setAssassinate(int assassinate) {
        this.assassinate = assassinate;
    }

    public int getCrit() {
        return crit;
    }

    public void setCrit(int crit) {
        this.crit = crit;
    }
}
