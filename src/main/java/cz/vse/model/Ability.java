package cz.vse.model;

/**
 * The Ability class represents the skill available for use in battle by the client's created character.
 *
 *@author     Jiri Mares
 *@since    november 2020
 */


public class Ability {
    private AbilityName abilityName;
    private String description;
    private int manaCost;
    private RoleName roleName;

    public Ability(AbilityName abilityName) {
        this.abilityName = abilityName;
        this.description = abilityName.description;
        this.manaCost = abilityName.manaCost;
        this.roleName = abilityName.roleName;
    }

    public AbilityName getAbilityName() {
        return abilityName;
    }

    public void setAbilityName(AbilityName abilityName) {
        this.abilityName = abilityName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getManaCost() {
        return manaCost;
    }

    public void setManaCost(int manaCost) {
        this.manaCost = manaCost;
    }

    public RoleName getRoleName() {
        return roleName;
    }

    public void setRoleName(RoleName roleName) {
        this.roleName = roleName;
    }
}
