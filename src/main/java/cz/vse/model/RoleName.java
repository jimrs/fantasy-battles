package cz.vse.model;

/**
 * The RoleName enum class represents the available roles.
 *
 *@author     Jiri Mares
 *@since    november 2020
 */

public enum RoleName {
    WARRIOR,
    ROGUE,
    KNIGHT,
    MAGE;

    /**
     * This is a quality-of-life method useful in checking if an user input is in correct format and if it is
     * one of the role names.
     *
     * @param test a String to check
     * @return true if the test string is an RoleName, false if not
     */
    public static boolean contains(String test) {

        for (RoleName rn : RoleName.values()) {
            if (rn.name().equals(test)) {
                return true;
            }
        }

        return false;
    }
}
