package cz.vse.model;

import cz.vse.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

/**
 *  The Battle class holds information about battle between 2 clients - connections.
 *  It handles tasks related to the battle and holds all information about it.
 *
 *@author     Jiri Mares
 *@since    november 2020
 */

public class Battle {

    private Connection conn1;
    private Connection conn2;
    private Connection winner; // a pointer to the connection that won the battle
    private Character turn;    // a pointer to the character which is on the turn at the moment
    private Player p1;
    private Player p2;
    private Character c1;
    private Character c2;
    private Character c1_tmp;
    private Character c2_tmp;
    final Logger log = LoggerFactory.getLogger(Connection.class);

    /**
     * This constructor is used when a battle is created. It sets some of the properties for player 1. We always
     * assume that player 1 is the player who has created the battle.
     *
     * @param conn connection to set the result of the battle in the Connection class property
     * @param p the player
     */
    public Battle(Connection conn, Player p) {
        this.conn1 = conn;
        this.p1 = p;
        this.c1 = p.getCharacter();
        this.c1_tmp = new Character(this.c1);   // deep copy
    }

    /**
     * This method is called by the player who creates the battle. It will put the calling thread to sleep,
     * and will be awoken when another player joins the battle.
     */
    public synchronized void waitForJoin() {
        try {
            log.info("User is waiting for a player to join.");
            conn1.getToClient().println("INFO; you are now waiting for a player to join ...");
            wait();    // 30 seconds is 30000
        } catch (InterruptedException e) {
            e.printStackTrace();
            log.error("handleCreateBattle WAIT error - user was probably tired of waiting and quit.");
        }
    }

    /**
     * This method is called by the player who joins the battle as player 2. It will set the properties needed
     * and notifyAll() to wake the sleeping thread of player 1, who has created the battle.
     *
     * @param conn connection to set the result of the battle in the Connection class property
     * @param p the player
     */
    public synchronized boolean join(Connection conn, Player p) {
        if (this.conn2 != null) {   // if conn2 is not null then someone has been faster than CONN at joining this battle
            return false;
        } else {
            this.conn2 = conn;
            this.p2 = p;
            this.c2 = p.getCharacter();
            this.c2_tmp = new Character(this.c2);   // deep copy

            notifyAll();
            return true;
        }
    }

    /**
     * This method is called before the battle to decide by random who will start.
     */
    public void prepare() {
        Random rn = new Random();
        int turn = rn.nextInt(2);   // bud 0 nebo 1
        if (turn == 0) {
            this.turn = this.c1;
            conn1.getToClient().println("INFO;" + this.c1.getName() + " is on the move.");
            conn2.getToClient().println("INFO;" + this.c1.getName() + " is on the move.");
        } else {
            this.turn = this.c2;
            conn1.getToClient().println("INFO;" + this.c2.getName() + " is on the move.");
            conn2.getToClient().println("INFO;" + this.c2.getName() + " is on the move.");
        }

        log.info(this.c1.getName() + " has " + this.c1.getHealth() + "HP and " + this.c1.getMana() + "MP.");
        log.info(this.c2.getName() + " has " + this.c2.getHealth() + "HP and " + this.c2.getMana() + "MP.");
        conn1.getToClient().println("INFO;" + this.c1.getName() + " has " + this.c1.getHealth() + "HP and " + this.c1.getMana() + "MP.");
        conn1.getToClient().println("INFO;" + this.c2.getName() + " has " + this.c2.getHealth() + "HP and " + this.c2.getMana() + "MP.");
        conn2.getToClient().println("INFO;" + this.c1.getName() + " has " + this.c1.getHealth() + "HP and " + this.c1.getMana() + "MP.");
        conn2.getToClient().println("INFO;" + this.c2.getName() + " has " + this.c2.getHealth() + "HP and " + this.c2.getMana() + "MP.");
    }

    /**
     * This method is called by every player to find out if they can play, i.e. if they are on the turn.
     *
     * @param player the Character who wants to play
     * @return true if the player is on the turn, false if not
     */
    public synchronized boolean canIPlay(Character player) {
        return turn.equals(player);
    }

    /**
     * This method is called every turn to see if any of the players have reached less than 0 health, in other words
     * if they are still alive or not.
     *
     * @return true if any player is dead, false everyone is alive
     */
    public synchronized boolean isItOver() {
        if (this.c1.getHealth() <= 0) {
            winner = conn2;
            return true;
        } else if (this.c2.getHealth() <= 0) {
            winner = conn1;
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method is called after every player action to change the player who is on turn. It also prints
     * every relevant information about the battle and sets the player statuses back to normal.
     */
    private void turn() {
        log.info(this.c1.getName() + " has " + this.c1.getHealth() + "HP and " + this.c1.getMana() + "MP.");
        log.info(this.c2.getName() + " has " + this.c2.getHealth() + "HP and " + this.c2.getMana() + "MP.");
        conn1.getToClient().println("INFO;" + this.c1.getName() + " has " + this.c1.getHealth() + "HP and " + this.c1.getMana() + "MP.");
        conn1.getToClient().println("INFO;" + this.c2.getName() + " has " + this.c2.getHealth() + "HP and " + this.c2.getMana() + "MP.");
        conn2.getToClient().println("INFO;" + this.c1.getName() + " has " + this.c1.getHealth() + "HP and " + this.c1.getMana() + "MP.");
        conn2.getToClient().println("INFO;" + this.c2.getName() + " has " + this.c2.getHealth() + "HP and " + this.c2.getMana() + "MP.");

        if (this.isItOver()) {
            log.info("The battle is over, someone is victorious.");
            conn1.getToClient().println("INFO;" + "The battle is over! Press enter to continue ...");
            conn2.getToClient().println("INFO;" + "The battle is over! Press enter to continue ...");
            return;
        }

        if (this.turn.equals(this.c1)) {    // here the round of C1 has just ended

            c2.setDefense(c2.getEndurance() * 3);   // the default value of defense

            c1.setCharged(false);

            if (c1.getAssassinate() == 0) {
                c1.setCrit(c1.getAgility() * 3);        // default crit
                conn1.getToClient().println("INFO;" + this.c1.getName() + " ASSASSINATE has ended!");
                conn2.getToClient().println("INFO;" + this.c1.getName() + " ASSASSINATE has ended!.");
                c1.setAssassinate(c1.getAssassinate() - 1);     // will be -1

            } else if (c1.getAssassinate() > 0)  {
                conn1.getToClient().println("INFO;" + this.c1.getName() + " ASSASSINATE remains for " + c1.getAssassinate() + " rounds.");
                conn2.getToClient().println("INFO;" + this.c1.getName() + " ASSASSINATE remains for " + c1.getAssassinate() + " rounds.");
                c1.setAssassinate(c1.getAssassinate() - 1);
            }

            if (!c2.isStunned()) {
                this.turn = this.c2;
                conn1.getToClient().println("INFO;" + this.c2.getName() + " is on the move.");
                conn2.getToClient().println("INFO;" + this.c2.getName() + " is on the move.");
            } else {
                conn1.getToClient().println("INFO;" + this.c1.getName() + " is on the move because " + this.c2.getName() + " is stunned.");
                conn2.getToClient().println("INFO;" + this.c1.getName() + " is on the move because " + this.c2.getName() + " is stunned.");
                c2.setStunned(false);

                c1.setCrit(c1.getAgility() * 3);    // bez tohoto by pri stunu neprobehlo snizeni crit na vychozi hodnotu, spravne by sem patril i setcharged false, ale vzhledem k tomu, ze stun jde jen ze skillu, neni to nutne
            }
        } else {        // here the round of C2 has just ended

            c1.setDefense(c1.getEndurance() * 3);   // the default value of defense

            c2.setCharged(false);

            if (c2.getAssassinate() == 0) {
                c2.setCrit(c2.getAgility() * 3);        // default crit
                conn1.getToClient().println("INFO;" + this.c2.getName() + " ASSASSINATE has ended!");
                conn2.getToClient().println("INFO;" + this.c2.getName() + " ASSASSINATE has ended!.");
                c2.setAssassinate(c2.getAssassinate() - 1);     // will be -1

            } else if (c2.getAssassinate() > 0) {
                conn1.getToClient().println("INFO;" + this.c2.getName() + " ASSASSINATE remains for " + c2.getAssassinate() + " rounds.");
                conn2.getToClient().println("INFO;" + this.c2.getName() + " ASSASSINATE remains for " + c2.getAssassinate() + " rounds.");
                c2.setAssassinate(c2.getAssassinate() - 1);
            }

            if (!c1.isStunned()) {
                this.turn = this.c1;
                conn1.getToClient().println("INFO;" + this.c1.getName() + " is on the move.");
                conn2.getToClient().println("INFO;" + this.c1.getName() + " is on the move.");
            } else {
                conn1.getToClient().println("INFO;" + this.c2.getName() + " is on the move because " + this.c1.getName() + " is stunned.");
                conn2.getToClient().println("INFO;" + this.c2.getName() + " is on the move because " + this.c1.getName() + " is stunned.");
                c1.setStunned(false);

                c2.setCrit(c2.getAgility() * 3);
            }
        }
    }

    /**
     * This method is called by the player who wants to attack the other player. It also prints the relevant
     * information to each player and then calls turn().
     */
    public void attack(Character player, boolean spell) {
        Character opponent;

        if (player.equals(this.c1)) {
            opponent = this.c2;
        } else {
            opponent = this.c1;
        }

        if (opponent.isRiposte()) {
            log.info(opponent.getName() + " has dodged the attack! RIPOSTE!");
            conn1.getToClient().println("INFO;" + opponent.getName() + " has dodged the attack! RIPOSTE!");
            conn2.getToClient().println("INFO;" + opponent.getName() + " has dodged the attack! RIPOSTE!");
            opponent.setRiposte(false);
            this.turn();
            return;
        }

        int newHP;
        int oldHP = opponent.getHealth();
        int attackValue;

        if (spell) {
            attackValue = player.getIntellect() * 10;
        } else {
            attackValue = player.getStrength() * 10;
        }


        Random rn = new Random();
        int critBase = rn.nextInt(100); // 0 az 99 random number
        if (critBase <= player.getCrit()) {
            attackValue = attackValue * 2;
            conn1.getToClient().println("INFO; CRIT!");
            conn2.getToClient().println("INFO; CRIT!");
        }

        attackValue = attackValue - opponent.getDefense();  // defense se odecita az po CRIT
        if (attackValue < 10) {
            attackValue = 10;       // minimalni damage, kdo kdy muze dat je 10
        }
        newHP = oldHP - attackValue;
        opponent.setHealth(newHP);

        log.info(opponent.getName() + " has been damaged for " + attackValue + " HP.");
        conn1.getToClient().println("INFO;" + opponent.getName() + " has been damaged for " + attackValue + " HP.");
        conn2.getToClient().println("INFO;" + opponent.getName() + " has been damaged for " + attackValue + " HP.");
        this.turn();
    }

    /**
     * This method is called by the player who wants to defend for the round. It also prints the relevant
     * information to each player and then calls turn().
     */
    public void defend(Character player) {

        player.setDefense(player.getDefense() * 2);

        log.info(player.getName() + " has used defend.");
        conn1.getToClient().println(player.getName() + " has used defend.");
        conn2.getToClient().println(player.getName() + " has used defend.");
        this.turn();
    }

    /**
     * This method is called by the player who wants to use his skill. It is a big method that takes care of
     * every possible skill, checks if the player has enough mana to use it, and works out the various mechanics.
     */
    public void skill(Character player) {
        if (player.isCharged()) {
            log.info(player.getName() + " has tried to use a skill while charged.");
            conn1.getToClient().println("ERROR;" + player.getName() + " has tried to use a skill but he's charged.");
            conn2.getToClient().println("ERROR;" + player.getName() + " has tried to use a skill but he's charged.");
            return;
        }

        Character opponent = null;

        if (player.equals(this.c1)) {
            opponent = this.c2;
        } else {
            opponent = this.c1;
        }

        String skillName = player.getSkill().getAbilityName().name();
        switch(skillName) {
            case "SHIELD_BASH":
                if (player.getMana() >= player.getSkill().getManaCost()) {
                    opponent.setStunned(true);
                    log.info(player.getName() + " has used SHIELD BASH.");
                    conn1.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ". The opponent is now stunned.");
                    conn2.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ". The opponent is now stunned.");
                    player.setMana(player.getMana() - player.getSkill().getManaCost());
                    this.attack(player, false);
                } else {
                    log.info(player.getName() + " has tried to use " + skillName + ". But not enough MP.");
                    conn1.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                    conn2.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                }
                break;
            case "CHARGE":
                if (player.getMana() >= player.getSkill().getManaCost()) {
                    opponent.setCharged(true);
                    log.info(player.getName() + " has used " + skillName + ".");
                    conn1.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ". The opponent cannot use skills for the next round.");
                    conn2.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ". The opponent cannot use skills for the next round.");
                    player.setMana(player.getMana() - player.getSkill().getManaCost());
                    this.turn();
                } else {
                    log.info(player.getName() + " has tried to use " + skillName + ". But not enough MP.");
                    conn1.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                    conn2.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                }
                break;
            case "RIPOSTE":
                if (player.getMana() >= player.getSkill().getManaCost()) {
                    player.setRiposte(true);
                    player.setCrit(100);
                    log.info(player.getName() + " has used " + skillName + ".");
                    conn1.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    conn2.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    player.setMana(player.getMana() - player.getSkill().getManaCost());
                    this.attack(player, false);
                } else {
                    log.info(player.getName() + " has tried to use " + skillName + ". But not enough MP.");
                    conn1.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                    conn2.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                }
                break;
            case "ASSASSINATE":
                if (player.getMana() >= player.getSkill().getManaCost()) {
                    player.setAssassinate(3);
                    player.setCrit(50);
                    log.info(player.getName() + " has used " + skillName + ".");
                    conn1.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    conn2.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    player.setMana(player.getMana() - player.getSkill().getManaCost());
                    this.turn();
                } else {
                    log.info(player.getName() + " has tried to use " + skillName + ". But not enough MP.");
                    conn1.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                    conn2.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                }
                break;
            case "DIVINE_SHIELD":
                if (player.getMana() >= player.getSkill().getManaCost()) {
                    player.setDefense(1000);
                    log.info(player.getName() + " has used " + skillName + ".");
                    conn1.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    conn2.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    player.setMana(player.getMana() - player.getSkill().getManaCost());
                    this.turn();
                } else {
                    log.info(player.getName() + " has tried to use " + skillName + ". But not enough MP.");
                    conn1.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                    conn2.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                }
                break;
            case "JUDGEMENT":
                if (player.getMana() >= player.getSkill().getManaCost()) {
                    player.setHealth(player.getHealth() + 50);
                    log.info(player.getName() + " has used " + skillName + ".");
                    conn1.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    conn2.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    player.setMana(player.getMana() - player.getSkill().getManaCost());
                    this.attack(player, false);
                } else {
                    log.info(player.getName() + " has tried to use " + skillName + ". But not enough MP.");
                    conn1.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                    conn2.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                }
                break;
            case "FIREBALL":
                if (player.getMana() >= player.getSkill().getManaCost()) {
                    log.info(player.getName() + " has used " + skillName + ".");
                    conn1.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    conn2.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    player.setMana(player.getMana() - player.getSkill().getManaCost());
                    this.attack(player, true);
                } else {
                    log.info(player.getName() + " has tried to use " + skillName + ". But not enough MP.");
                    conn1.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                    conn2.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                }
                break;
            case "DRAGON_BREATH":
                if (player.getMana() >= player.getSkill().getManaCost()) {
                    opponent.setStunned(true);
                    player.setCrit(50);
                    log.info(player.getName() + " has used " + skillName + ".");
                    conn1.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    conn2.getToClient().println("INFO;" + player.getName() + " has used " + skillName + ".");
                    player.setMana(player.getMana() - player.getSkill().getManaCost());
                    this.attack(player, true);
                } else {
                    log.info(player.getName() + " has tried to use " + skillName + ". But not enough MP.");
                    conn1.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                    conn2.getToClient().println("ERROR;" + player.getName() + " has tried to use " + skillName + ". But not enough MANA.");
                }
                break;
        }
    }

    /**
     * This method is called by the player who wants to resign. The other player will be set to a winner.
     *
     * @param player the Character who calls the method and wants to resign
     */
    public void resign(Character player) {

        if (player.equals(this.c1)) {
            this.winner = this.conn2;
        } else {
            this.winner = this.conn1;
        }

        player.setHealth(0);

        log.info(player.getName() + " has resigned.");
        conn1.getToClient().println(player.getName() + " has resigned.");
        conn2.getToClient().println(player.getName() + " has resigned.");
        this.turn();
    }

    /**
     * This method is called after the battle to reset the player characters to their original state.
     */
    public void postFight() {
        p1.setCharacter(this.c1_tmp);   // shallow copy
        p2.setCharacter(this.c2_tmp);
    }

    public Connection getWinner() {
        return winner;
    }

    public void setWinner(Connection winner) {
        this.winner = winner;
    }

    public Connection getConn1() {
        return conn1;
    }

    public void setConn1(Connection conn1) {
        this.conn1 = conn1;
    }

    public Connection getConn2() {
        return conn2;
    }

    public void setConn2(Connection conn2) {
        this.conn2 = conn2;
    }

    public Character getTurn() {
        return turn;
    }

    public void setTurn(Character turn) {
        this.turn = turn;
    }

    public Player getP1() {
        return p1;
    }

    public void setP1(Player p1) {
        this.p1 = p1;
    }

    public Player getP2() {
        return p2;
    }

    public void setP2(Player p2) {
        this.p2 = p2;
    }

    public Character getC1() {
        return c1;
    }

    public void setC1(Character c1) {
        this.c1 = c1;
    }

    public Character getC2() {
        return c2;
    }

    public void setC2(Character c2) {
        this.c2 = c2;
    }

    public Character getC1_tmp() {
        return c1_tmp;
    }

    public void setC1_tmp(Character c1_tmp) {
        this.c1_tmp = c1_tmp;
    }

    public Character getC2_tmp() {
        return c2_tmp;
    }

    public void setC2_tmp(Character c2_tmp) {
        this.c2_tmp = c2_tmp;
    }
}
