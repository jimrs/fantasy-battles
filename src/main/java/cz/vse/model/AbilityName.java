package cz.vse.model;

import java.io.IOException;

/**
 * The AbilityName enum class holds information about each available ability, together with it's mana cost and required role.
 *
 *@author     Jiri Mares
 *@since    november 2020
 */

public enum AbilityName {
    SHIELD_BASH("Raise your shield, damaging and stunning your opponent.", 30, RoleName.WARRIOR),
    CHARGE("Rush to your opponent with surprise, causing him to not be able to use any skills for the next round.", 20, RoleName.WARRIOR),

    RIPOSTE("Dodge the next attack and deal a critical strike to your opponent's weak spot.", 30, RoleName.ROGUE),
    ASSASSINATE("You focus on your opponent's weak spots, increasing your critical chance for your next 3 attacks.", 20, RoleName.ROGUE),

    DIVINE_SHIELD("Call on the power of God to shield yourself from all damage for the next attack.", 60, RoleName.KNIGHT),
    JUDGEMENT("Judge your opponent's sins in the name of God, dealing damage and healing yourself.", 30, RoleName.KNIGHT),

    FIREBALL("Hurl a fiery ball at your opponent, dealing fire damage.", 20, RoleName.MAGE),
    DRAGON_BREATH("Stun your opponent and deal a massive amount of damage.", 100, RoleName.MAGE)
    ;

    public final String description;
    public final int manaCost;
    public final RoleName roleName;

    AbilityName(String description, int manaCost, RoleName roleName) {
        this.description = description;
        this.manaCost = manaCost;
        this.roleName = roleName;
    }

    /**
     * This is a quality-of-life method useful in checking if an user input is in correct format and if it is
     * one of the ability names.
     *
     * @param test a String to check
     * @return true if the test string is an AbilityName, false if not
     */
    public static boolean contains(String test) {

        for (AbilityName ab : AbilityName.values()) {
            if (ab.name().equals(test)) {
                return true;
            }
        }

        return false;
    }
}
