package cz.vse.model;

/**
 * The Player class represents the client's connection on a less abstract level - the client is not treated
 * as a connection but as a player with username, password, and his own character.
 *
 *@author     Jiri Mares
 *@since    november 2020
 */

public class Player {
    private String username;
    private String password;
    private Character character;

    public Player(String username, String password) {
        this.username = username;
        this.password = password;
        this.character = null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }
}
