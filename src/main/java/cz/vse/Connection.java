package cz.vse;

import java.io.*;
import java.net.Socket;

import cz.vse.model.*;
import cz.vse.model.Character;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  The Connection class represents each client connection. It takes care of client communication and holds
 *  information about the client's username, password, created character, battles.
 *
 *@author     Jiri Mares
 *@since    november 2020
 */

public class Connection implements Runnable {
    private Socket socket;
    private Player player;
    private Boolean isLoggedIn = false;
    private Boolean quitSent = false;
    public volatile Boolean lookingForBattle = false;   // pristupuje zde vic vlaken
    public Boolean wonBattle = false;
    public Battle battle = null;

    private Listener listener;
    private BufferedReader fromClient;
    private PrintWriter toClient;
    final Logger log = LoggerFactory.getLogger(Connection.class);

    public Connection(Listener listener) {
        this.player = null;
        this.listener = listener;
    }

    @Override
    public void run() {
        try {
            communicateWithUser();

        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error inside run method of Connection thread (startCommunication() threw IOException)!");
        }
    }

    /**
     * This method handles (almost) all communication with the user. Input and output streams are created,
     * a while cycle with readLine() method takes care of all user commands and transfers the control flow to
     * the appropriate method.
     *
     * @throws IOException when the toClient PrintWriter or fromClient BufferedReader gets an error
     */
    private void communicateWithUser() throws IOException {
        fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        toClient = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"), true);

        log.info("Starting communication with new client ...");
        toClient.println("OK; Connection successful! Welcome! Please login using your username and password ...");

        while (!quitSent) {
            String incomingMessage = fromClient.readLine();
            incomingMessage = incomingMessage.trim();
            if (player != null) {
                log.info("{}: {}", player.getUsername(), incomingMessage);
            } else {
                log.info("Anonymous: {}", incomingMessage);
            }

            if (isMessageCorrectForLogin(incomingMessage)) {
                    String[] incomingMessageSplit = incomingMessage.split("\\s+");
                    this.handleLogin(incomingMessageSplit);

            } else if (isLoggedIn) {    // only if the user has logged in can he try to create character and find battle

                switch (incomingMessage) {
                    case "CHAR":
                        this.handleCharacterCreation();
                        break;
                    case "BATTLE FIND":
                        this.handleFindBattle();
                        break;
                    case "BATTLE CREATE":
                        this.handleCreateBattle();
                        break;
                    case "QUIT":
                        this.handleQuit();
                        break;
                    case "ABOUT":
                        this.handleAbout();
                        break;
                    default:
                        log.warn("User command has a wrong format.");
                        toClient.println("ERROR; incorrect command format");
                }

            } else {    // these commands are available even if the user hasn't logged in

                switch (incomingMessage) {
                    case "QUIT":
                        this.handleQuit();
                        break;
                    case "ABOUT":
                        this.handleAbout();
                        break;
                    default:
                        log.warn("User command has a wrong format.");
                        toClient.println("ERROR; incorrect command format. maybe you need to login first?");
                }
            }
        }
    }

    /**
     * This method handles the QUIT command by user. It sets up all variables back to default and nulls out
     * everything it can. Also closes the connections.
     */
    private void handleQuit() {
        log.info("User has sent a QUIT command.");
        toClient.println("OK; You have been disconnected from the server.");

        quitSent = true;
        isLoggedIn = false;
        lookingForBattle = false;
        battle = null;
        player = null;
        this.listener.removeFromConnectionList(this);

        toClient.close();
        try {
            fromClient.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error while handling user quit request.");
        }
    }

    /**
     * This method handles the ABOUT command by user. It sends a message to the user with relevant information
     * about the server. Number of logged in users, their names, players looking for battle, and the user's
     * own character.
     */
    private void handleAbout() {
        log.info("User has sent an ABOUT command.");

        String reply = "";
        if (isLoggedIn) {
            reply += "INFO; You are logged in as " + player.getUsername() + ".\n";
            if (player.getCharacter() != null) {
                reply += "Your character is named " + player.getCharacter().getName() + ".\n";
            }
        }

        reply += "There are " + listener.countUsers() + " users connected to the server.\n";
        reply += "The users connected are:\n";
        reply += listener.getAllUsernames() + ".\n";

        reply += "Users looking for a battle right now are:\n";
        reply += listener.getAllLookingForBattle() + ".\n";


        reply += "\nCreate your own character and battle your opponents in epic fights online!\n";

        toClient.printf(reply);
    }

    /**
     * This method handles the LOGIN command by user. It checks if the user is already logged in or if the
     * username is already taken.
     *
     * @param incomingMessageSplit is an array of Strings split to [0]LOGIN, [1]USERNAME, [2]PASSWORD
     */
    private void handleLogin(String[] incomingMessageSplit) {

        log.info("Starting the logging in process ...");

        String newUsername = incomingMessageSplit[1];
        String newPassword = incomingMessageSplit[2];
        String otherUsers = listener.getAllUsernames();

        if (!isLoggedIn) {
            if (otherUsers.contains(newUsername)) {
                log.warn("User has tried to login with an already taken username.");
                toClient.println("ERROR; Desired username is already taken.");

            } else {
                player = new Player(newUsername, newPassword);
                log.info("User {} logged in successfully.", newUsername);
                toClient.println("OK; You have successfully logged in.");
                isLoggedIn = true;
            }
        } else {
            log.warn("User was already logged in.");
            toClient.println("ERROR; You are already logged in.");
        }
    }

    /**
     * This method handles the CHAR command by user. It starts a new communication while cycle and takes the user
     * through the process of character creation. User can choose a name, a role, and a skill for his character.
     *
     * @throws IOException fromClient.readLine() can throw an exception
     */
    private void handleCharacterCreation() throws IOException {
        log.info("Starting the character creation process ...");
        toClient.println("OK; Please create your character ...");

        Character newCharacter = new Character();
        boolean exitSent = false; // user has the option to cancel character creation

        while (!newCharacter.isAllSet() && !exitSent) {
            String incomingMessage = fromClient.readLine();
            log.info("{}: {}", player.getUsername(), incomingMessage);

            String[] incomingMessageSplit = incomingMessage.split("\\s+");

            switch (incomingMessageSplit[0]) {  // switch used with a String as a parameter uses the .equals() method, so all good
                case "NAME":
                    if (incomingMessageSplit.length == 2) {
                        log.info("User has set a name to his character.");
                        toClient.println("OK; Your character's name has been set.");
                        newCharacter.setName(incomingMessageSplit[1]);
                    } else {
                        log.warn("User has failed to set a name to his character.");
                        toClient.println("ERROR; You have chosen a wrong name (empty string).");
                    }
                    break;

                case "ROLE":
                    if (incomingMessageSplit.length == 2
                        && RoleName.contains(incomingMessageSplit[1])) {

                        newCharacter.setRole(new Role(RoleName.valueOf(incomingMessageSplit[1])));
                        log.info("User has set a role to his character.");
                        toClient.println("OK; Your character's role has been set.");
                    } else {
                        log.warn("User has failed to set a role to his character.");
                        toClient.println("ERROR; You have chosen a wrong role for your character (WARRIOR, ROGUE, KNIGHT, MAGE).");
                    }
                    break;

                case "SKILL":
                    if (incomingMessageSplit.length == 2
                        && AbilityName.contains(incomingMessageSplit[1])) {

                        Ability ab = new Ability(AbilityName.valueOf(incomingMessageSplit[1]));
                        if (newCharacter.getRole() != null
                            && ab.getRoleName().equals(newCharacter.getRole().getRoleName())) {
                            newCharacter.setSkill(ab);
                            log.info("User has set a skill to his character.");
                            toClient.println("OK; Your character's skill has been set.");
                        } else {
                            log.warn("User has failed to set a skill to his character.");
                            toClient.println("ERROR; Your character's skill has not been set. First set your role, then you need to choose a skill for that role.");
                        }
                    } else {
                        log.warn("User has failed to set a skill to his character.");
                        toClient.println("ERROR; Your character's skill has not been set. You have chosen a wrong skill.");
                    }
                    break;

                case "EXIT":
                    log.info("User has sent CHAR exit to end character creation.");
                    toClient.println("OK; Character creation process has been exited.");
                    exitSent = true;
                    break;

                default:
                    log.warn("Failed to create character.");
                    toClient.println("ERROR; failed to create character, wrong message format.");
            }
        }

        if (!exitSent) {    // only if the creation process wasn't interrupted
            log.info("Character successfully created!");
            toClient.println("INFO; character successfully created.");
            player.setCharacter(newCharacter);
        }
    }

    /**
     * This method handles the BATTLE FIND command by user. It first checks if the player is eligible to find
     * a battle and then looks for players who have created a battle and are waiting for an opponent.
     * After it finds someone, it transfers control to fightBattle and depending on the result, informs the user
     * about a win or a loss.
     *
     * @throws IOException fightBattle can throw an exception and it is delegated here
     */
    private void handleFindBattle() throws IOException {
        if (this.player != null
            && this.player.getCharacter() != null
            && this.player.getCharacter().isAllSet()
            && !this.lookingForBattle
            && this.battle == null) {

            for (Connection conn : this.listener.getConnectionList()) {
                if (conn.lookingForBattle) {

                    if (conn.battle.join(this, this.player)) {
                        conn.lookingForBattle = false;
                        this.battle = conn.battle;
                        this.battle.prepare();
                        this.wonBattle = this.fightBattle();

                        if (this.wonBattle) {
                            log.info("User has won a battle.");
                            toClient.println("INFO; you have just won the battle. Returning to main menu.");
                        } else {
                            log.info("User has lost a battle.");
                            toClient.println("INFO; you have just lost the battle. Returning to main menu.");
                        }

                        return;

                    } else {
                        log.warn("Failed to join a battle.");
                        toClient.println("ERROR; failed to join a battle because someone was faster than you.");
                    }
                }
            }

            log.warn("Failed to join a battle because no players are looking for it.");
            toClient.println("ERROR; failed to join a battle because no players want to battle. Try creating a battle!");

        } else {
            log.warn("user has failed to find a battle because he didn't meet the requirements.");
            toClient.println("ERROR; you do not meet requirements to find a battle. maybe you didn't set a character yet?");
        }
    }

    /**
     * This method handles the BATTLE CREATE command by user. It first checks if the player is eligible to create
     * a battle and then makes a new Battle class instance, calls it's method waitForJoin and the thread goes to sleep,
     * until a player joins and wakes up the thread. Then it transfers control to fightBattle and depending
     * on the result, informs the user about a win or a loss.
     *
     * @throws IOException fightBattle can throw an exception and it is delegated here
     */
    private void handleCreateBattle() throws IOException {
        if (this.player != null
            && this.player.getCharacter() != null
            && this.player.getCharacter().isAllSet()
            && !this.lookingForBattle
            && this.battle == null) {

            this.battle = new Battle(this, this.player);
            this.lookingForBattle = true;
            this.battle.waitForJoin();

            this.wonBattle = this.fightBattle();

            if (this.wonBattle) {
                log.info("User has won a battle.");
                toClient.println("INFO; you have just won the battle. Returning to main menu.");
            } else {
                log.info("User has lost a battle.");
                toClient.println("INFO; you have just lost the battle. Returning to main menu.");
            }
        } else {
            log.warn("user has failed to create a battle because he didn't meet the requirements.");
            toClient.println("ERROR; you do not meet requirements to create a battle. maybe you didn't set a character yet?");
        }
    }

    /**
     * This method handles the BATTLE CREATE command by user. It first checks if the player is eligible to create
     * a battle and then makes a new Battle class instance, calls it's method waitForJoin and the thread goes to sleep,
     * until a player joins and wakes up the thread. Then it transfers control to fightBattle and depending
     * on the result, informs the user about a win or a loss.
     *
     * @throws IOException fightBattle can throw an exception and it is delegated here
     */
    private boolean fightBattle() throws IOException {
        log.info("Starting a battle ...");
        toClient.println("INFO; IT'S A FIGHT!");
        boolean resign = false;

        while (!resign) {
            String incomingMessage = fromClient.readLine();

            if (this.battle.isItOver()) {  // in case this connection is waiting at READLINE, and the opponent RESIGNES
                break;
            }

            log.info("{}: {}", player.getUsername(), incomingMessage);
            if (this.battle.canIPlay(this.player.getCharacter())) {

                switch (incomingMessage) {
                    case "ATT":
                        this.battle.attack(this.player.getCharacter(), false);
                        break;
                    case "DEF":
                        this.battle.defend(this.player.getCharacter());
                        break;
                    case "SKILL":
                        this.battle.skill(this.player.getCharacter());
                        break;
                    case "RESIGN":
                        resign = true;
                        this.battle.resign(this.player.getCharacter());
                        break;
                    default:
                        log.warn("User has input a wrong command in BATTLE mode.");
                        toClient.println("ERROR; Wrong message format.");
                        break;
                }
            } else if (incomingMessage.equals("RESIGN")) {
                resign = true;
                this.battle.resign(this.player.getCharacter());

            } else {
                log.warn("User is not on the turn but tried to play.");
                toClient.println("ERROR; you cannot play yet, it's the opponent's turn.");
            }
        }

        this.battle.postFight();
        if (this.battle.getWinner().equals(this)) {
            this.battle = null;
            return true;
        } else {
            this.battle = null;
            return false;
        }
    }

    /**
     * This method is only to check if the three-word command LOGIN USERNAME PASSWORD is in correct format.
     * It is used to avoid too much unnecessary ifs and code in the communicateWithUser method.
     *
     * @param message is a String to be checked for correctness
     * @return true if the message is ok, false if not
     */
    private static Boolean isMessageCorrectForLogin(String message) {

        if (message.contains(" ")) {
            String[] messageSplit = message.split("\\s+");

            if (messageSplit.length == 3
                    && messageSplit[0] != null
                    && messageSplit[1] != null
                    && messageSplit[2] != null
                    && messageSplit[0].equals("LOGIN")) {

                return true;
            }
        }

        return false;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public BufferedReader getFromClient() {
        return fromClient;
    }

    public void setFromClient(BufferedReader fromClient) {
        this.fromClient = fromClient;
    }

    public PrintWriter getToClient() {
        return toClient;
    }

    public void setToClient(PrintWriter toClient) {
        this.toClient = toClient;
    }

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Boolean getLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public Boolean getQuitSent() {
        return quitSent;
    }

    public void setQuitSent(Boolean quitSent) {
        this.quitSent = quitSent;
    }

    public Boolean getLookingForBattle() {
        return lookingForBattle;
    }

    public void setLookingForBattle(Boolean lookingForBattle) {
        this.lookingForBattle = lookingForBattle;
    }

    public Boolean getWonBattle() {
        return wonBattle;
    }

    public void setWonBattle(Boolean wonBattle) {
        this.wonBattle = wonBattle;
    }

    public Battle getBattle() {
        return battle;
    }

    public void setBattle(Battle battle) {
        this.battle = battle;
    }
}