package cz.vse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *  The main class. The server gets started here on the port 1099.
 *  Then the control flow transfers to the Listener class and its method acceptConnections().
 *
 *@author     Jiri Mares
 *@since    november 2020
 */

public class Main {
    final static Logger log = LoggerFactory.getLogger(Listener.class);

    public static void main(String[] args) {
        log.info("Server started.");

        Listener listener = new Listener(1099);

        try {
            listener.acceptConnections();
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error at main thread, IOException out of acceptConnections.");
        }

        log.info("Server terminated.");
    }
}